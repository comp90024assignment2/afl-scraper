#!/usr/bin/env python3
from afl_tables import MatchScraper
from afl_tables.cli import to_serializable
from cloudant import couchdb
import argparse


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', help='CouchDB URL', required=True)
    parser.add_argument('--username', help='CouchDB username', required=True)
    parser.add_argument('--password', help='CouchDB password', required=True)
    parser.add_argument('--start', type=int, help='Year to start scraping (inclusive)', required=True)
    parser.add_argument('--end', type=int, help='Year to stop scraping (exclusive)', required=True)
    return parser.parse_args()


args = get_args()
with couchdb(args.username, args.password, url=args.url, connect=True) as couch:
    db = couch['comp90024']

    for year in range(args.start, args.end):
        docs = []
        for round in MatchScraper.scrape(year):
            for match in round.matches:
                match_json = to_serializable(match)
                match_json['type'] = 'match'
                match_json['_id'] = f'{to_serializable(match.date)}: {match}'
                docs.append(match_json)
        db.bulk_docs(docs)
